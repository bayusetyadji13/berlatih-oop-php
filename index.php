<?php
require "animal.php";
require "ape.php";
require "frog.php";

$sheep  = new animal("Shaun");
$kodok = new frog("buduk");
$sungokong = new ape("kera sakti");

echo $sheep->name;
echo "<br>";
echo $sheep->legs;
echo "<br>";
echo $sheep->cold_blooded;
echo "<br>";
echo "<br>";
echo $sungokong->name;
echo "<br>";
echo $sungokong->legs;
echo "<br>";
echo $sungokong->yell();
echo "<br>";
echo "<br>";
echo $kodok->name;
echo "<br>";
echo $kodok->legs;
echo "<br>";
echo $kodok->jump();
